Clone Dockerfile

    git clone https://botChun@bitbucket.org/botChun/golang_dockerfile.git


Build Image

    cd golang_dockerfile
    docker build -t golang:1.11.4 .



Build Container

    docker run -v /home/.go:/home/.go -v /home/.logs:/root/.logs -d -p 31222:22 --name golang golang:1.11.4  /root/start.sh